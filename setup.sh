#!/bin/bash

FORCE_V1_SCRIPT=force_cgroup_v1.sh

if [ ! -d /sys/fs/cgroup/memory ]; then
	while true; do
	read -p "Legacy memory cgroup not found. Force lecacy only mount on next boot?" yn
		case $yn in
			[Yy]* ) ./${FORCE_V1_SCRIPT}; break ;;
			[Nn]* ) echo "Ok. See ${FORCE_V1_SCRIPT} if you change your mind."; break;;
			* ) echo "Please reply with yes or no";;
		esac
	done
fi

if ! command -v gdb &> /dev/null
then
	while true; do
	read -p "gdb not found. Install?" yn
		case $yn in
			[Yy]* ) dnf install -y gdb; break ;;
			[Nn]* ) echo "Ok. The reproducer will not work :)"; break;;
			* ) echo "Please reply with yes or no";;
		esac
	done
fi
