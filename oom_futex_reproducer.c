#define _GNU_SOURCE
#include <errno.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <stdnoreturn.h>

#define MEMCG_BASE "/sys/fs/cgroup/memory/"
#define MEMCG_PATH MEMCG_BASE "oom_futex_reproducer"

/* default to a single forker */
#define DEFAULT_NR_FORKERS 1

/* default to a single robust lock */
#define DEFAULT_NR_ROBUST_LOCKS 1

/* default to three private locks for our greedy child */
#define DEFAULT_NR_PRIVATE_LOCKS 3

/* default greediness of one for one touch of the memory per lock cycle */
#define DEFAULT_GREEDINESS 1

/* default to sleeping for five seconds before joining threads */
#define DEFAULT_SLEEPINESS 5

/* default to no verbose output */
#define DEFAULT_VERBOSITY false

static struct {
	bool thread_should_stop;
	bool do_not_touch;
	bool one_oom_many_locks;
	bool verbosity;
	pthread_mutex_t * stop_lock;
	pthread_mutex_t ** robust_locks;
	size_t nr_robust_locks;
	unsigned long totalram;
	unsigned long freeram;
	long pagesize;
	FILE * master_pid_file;
	size_t nr_forkers;
	size_t nr_private_locks;
	size_t greediness;
	int sleepiness;
	pthread_t * forkers;
	struct forker_tharg * forker_thargs;
} G = { 
	false,
	false,
	false,
	DEFAULT_VERBOSITY,
	NULL,
	NULL,
	DEFAULT_NR_ROBUST_LOCKS,
	0,
	0,
	0,
	NULL,
	DEFAULT_NR_FORKERS,
	DEFAULT_NR_PRIVATE_LOCKS,
	DEFAULT_GREEDINESS,
	DEFAULT_SLEEPINESS,
	NULL,
	NULL,
}; /* Global */

#define four_kilobytes (1L << 12)
#define sixty_four_megabytes (1L << 26)


static bool thread_should_stop(void)
{
	bool ret;

	pthread_mutex_lock(G.stop_lock);
	ret = G.thread_should_stop;
	pthread_mutex_unlock(G.stop_lock);

	return ret;
}

static void set_thread_should_stop(void)
{
	pthread_mutex_lock(G.stop_lock);
	G.thread_should_stop = true;
	pthread_mutex_unlock(G.stop_lock);
}

static bool directory_exists(char * path) {
	struct stat stats;
	
	if (!stat(path, &stats) && S_ISDIR(stats.st_mode)) {
		return true;
	}
	
	return false;

}

static bool legacy_memcg_available(void)
{
	// If MEMCG_BASE folder exists, we have legacy memcg
	return directory_exists(MEMCG_BASE);
}

static bool running_as_root(void)
{
	return getuid() == 0;
}

static int set_oom_score_adj(int score)
{
	FILE *fp;

	if (!(fp = fopen("/proc/self/oom_score_adj", "w"))) {
		perror("failed to open /proc/self/oom_score_adj");
		return 1;
	}

	fprintf(fp, "%d\n", score);

	if (ferror(fp)) {
		printf("Failed to write to proc filesystem and adjust pid %d's oom score\n",
				getpid());
		fclose(fp);
		return 1;
	}

	fclose(fp);

	return 0;
}

static int mmap_anon_shared(void ** mem, size_t size)
{
	
	if (!mem)
		return 1;

	*mem = mmap(0, size, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
	if (*mem == MAP_FAILED) {
		*mem = NULL;
		perror("Failed to do anon|shared mmap()");
		return 1;
	}
	return 0;
}

static int setup_robust_lock(pthread_mutex_t ** lock_ptr)
{
	pthread_mutexattr_t attr;

	if (!lock_ptr)
		return 1;


	/* 
	 * we need shared memory between parent and children
	 * 
	 * I made the decision to mmap each lock seperately to see what happens - Joel
	 */
	if (mmap_anon_shared((void **)lock_ptr, sizeof(pthread_mutex_t))) {
		// error message in above function
		return 1;
	}

	if (pthread_mutexattr_init(&attr)) {
		perror("Failed to init mutexattr");
		return 1;
	}

	
	if (pthread_mutexattr_setrobust(&attr, PTHREAD_MUTEX_ROBUST)) {
		perror("Unable to set ROBUST mutexattr");
		return 1;
	}

	if (pthread_mutex_init(*lock_ptr, &attr)) {
		perror("Failed to initialize main lock");
		abort();
	}

	if (pthread_mutexattr_destroy(&attr)) {
		perror("Failed to destroy mutexattr");
		return 1;
	}

	return 0;
}

static int robust_lock(size_t index)
{
	if (index >= G.nr_robust_locks) {
		/* printf("robust_lock called on out of bounds index\n"); */
		return 1;
	}

	switch (pthread_mutex_lock(G.robust_locks[index])) {
	case EOWNERDEAD:
		/* Someone died while holding this robust mutex */
		printf("[%d] Obtained lock from robbing grave of last owner\n", getpid()) ;
		if (pthread_mutex_consistent(G.robust_locks[index])) {
			perror("pthread_mutex_consistent failed");
			return 1;
		}
	case 0:
		return 0;
	default:
		perror("pthread_mutex_lock failed");
		return 1;
	}

}

static int robust_unlock(size_t index)
{
	if (index >= G.nr_robust_locks) {
		/* printf("robust_unlock called on out of bounds index\n"); */
		return 1;
	}

	if (pthread_mutex_unlock(G.robust_locks[index])) {
		perror("pthread_mutex_unlock failed");
		return 1;
	}

	return 0;
}

static int setup_memcg(void)
{
	FILE *fp;
	long memcg_limit = (long)G.freeram * 0.5; /* 50% of freeram */

	if (directory_exists(MEMCG_PATH))
		if (rmdir(MEMCG_PATH)) {
			perror("Failed to remove " MEMCG_PATH);
			return 1;
		}

	if (mkdir(MEMCG_PATH, 0755) < 0) {
		perror("mkdir " MEMCG_PATH " failed");
		return 1;
	};

	if (!(fp = fopen(MEMCG_PATH "/memory.limit_in_bytes", "w"))) {
		perror("fopen failed");
		return 1;
	}

	fprintf(fp, "%ld\n", memcg_limit);

	if (ferror(fp)) {
		printf("failed to write to <memcg>/memory.limit_in_bytes\n");
		return 1;
	}

	fclose(fp);

	// Only 64M of swap beyond memory limit allowed
	if (!(fp = fopen(MEMCG_PATH "/memory.memsw.limit_in_bytes", "w"))) {
		perror("fopen failed");
		return 1;
	}


	fprintf(fp, "%ld\n", memcg_limit + sixty_four_megabytes);
	
	if (ferror(fp)) {
		printf("failed to write to <memcg>/memory.memsw.limit_in_bytes\n");
		return 1;
	}

	fclose(fp);

	return 0;
}

static void cleanup_memcg(void)
{
	rmdir(MEMCG_PATH);
}

static int join_memcg(void)
{
	FILE *fp;

	if (!(fp = fopen(MEMCG_PATH "/cgroup.procs", "w"))) {
		perror("failed to open <memcg>/cgroup.procs");
		return 1;
	}

	fprintf(fp, "%d\n", getpid());

	if (ferror(fp)) {
		printf("failed to write current PID to <memcg>/cgroup.procs\n");
		fclose(fp);
		return 1;
	}

	fclose(fp);
	return 0;
}

static void touch_memory(char *a, long index)
{
	if (G.do_not_touch)
		return;

	*(a + (index * G.pagesize)) = '0';
}


enum child_type {
	CHILD_NORMAL = 0, 	/* lock 0, touch memory, oom_adj 999 */
	CHILD_GREEDY, 		/* lock 0..n,touch memory, oom_adj 999 */
	CHILD_CONTEND, 		/* lock i, do not touch memory, no oom_adj  */
};

const char * child_type_strings[] = {
	[CHILD_NORMAL] = "Child Normal",
	[CHILD_GREEDY] = "Child Greedy",
	[CHILD_CONTEND] = "Child Contend",
};

struct forker_tharg {
	enum child_type type;
	size_t lock_index; 	/* ignored unless type == CHILD_CONTEND */
};

noreturn static void child_main(enum child_type type, size_t lock_index)
{
	unsigned long long page_index_to_touch;
	char * big_chunk_of_mem;
	int i;

	printf("[%d] Start child with type [%s] lock_index [%zu]\n",
			getpid(), child_type_strings[type], lock_index);

	/*
	 * Join our memory cgroup
	 */
	if (join_memcg()) {
		printf("[%d] Unable to join memory cgroup. Child dies.\n", getpid());
		exit(1);
	}

	if (type == CHILD_CONTEND) {
		/* contend but do not touch */
		G.do_not_touch = true;
		/* skip oom adj and mmapping */
		goto loop;
	}
	/*
	 * Map as much memory as there is ram available
	 */
	big_chunk_of_mem = mmap(NULL, G.totalram, PROT_READ|PROT_WRITE,
			MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);

	if (big_chunk_of_mem == MAP_FAILED) {
		perror("private mmap in child failed");
		exit(1);
	}

	/*
	 * Maximum oom likelihood
	 */
	if (set_oom_score_adj(999))
		exit(1);

	if (type == CHILD_GREEDY)
		goto greedy_loop;

loop:
	/*
	 * Touch as many pages as possible while contending for a shared robust futex-backed lock
	 */
	for (page_index_to_touch = 0;;page_index_to_touch++) {
		robust_lock(lock_index);
		touch_memory(big_chunk_of_mem, page_index_to_touch);
		robust_unlock(lock_index);
	}

greedy_loop:

	pthread_mutex_t * private_locks;

	private_locks = mmap(NULL, sizeof(pthread_mutex_t) * G.nr_private_locks, PROT_READ|PROT_WRITE,
			MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);

	if (private_locks == MAP_FAILED) {
		perror("private lock mmap in child failed");
		exit(1);
	}

	for (i = 0; i < G.nr_private_locks; i++)
		if (pthread_mutex_init(private_locks + i, NULL)) {
			perror("mutex init failed");
			exit(1);
		}

	for (page_index_to_touch = 0;;page_index_to_touch++) {
		for (i = 0; i < G.nr_private_locks; i++)
			pthread_mutex_lock(private_locks + i);

		for (i = 0; i < G.nr_robust_locks; i++)
			robust_lock(i);

		for (i = 0; i < G.greediness; i++)
			touch_memory(big_chunk_of_mem, page_index_to_touch);

		for (i = G.nr_robust_locks - 1; i >= 0; i--)
			robust_unlock(i);

		for (i = G.nr_private_locks - 1; i >= 0; i--)
			pthread_mutex_unlock(private_locks + i);
	}
}

static void * forker_main(void *tharg_)
{
	int status, ret0 = 0, ret1 = 1;
	struct forker_tharg * tharg = (struct forker_tharg *)tharg_;
	pid_t pid;

	do {
		switch (pid = fork()) {
		case -1:
			perror("fork");
			pthread_exit(&ret1);
		case 0:
			/* does not return */
			child_main(tharg->type, tharg->lock_index);
		default:
			break;
		}

		if (waitpid(pid, &status, 0) < 0) {
			perror("waitpid failed");
			pthread_exit(&ret1);
		}

	} while (!thread_should_stop());

	pthread_exit(&ret0);
}

static int create_threads(void)
{
	int i, nr_threads;

	/*
	 * If we have one oom thread with many locks, we want one
	 * thread to cover each lock and one more to do the ooming.
	 */
	nr_threads = G.nr_forkers + (G.one_oom_many_locks ? 1 : 0);

	/*
	 * if we don't modify the thargs from the intial zeroed state
	 * each forker will be normal and contend lock 0
	 */
	if (!(G.forker_thargs = (struct forker_tharg *)calloc(
					nr_threads, sizeof(struct forker_tharg)))) {
		perror("malloc failed\n");
		return 1;
	}

	G.thread_should_stop = false;

	if (!(G.stop_lock = malloc(sizeof(pthread_mutex_t)))) {
		perror("malloc of pthread_mutex failed");
		return 1;
	}

	if (pthread_mutex_init(G.stop_lock, NULL)) {
		perror("mutex_init failed");
		return 1;
	}
	
	for (i = 0; i < nr_threads ; i++) {
		if (G.one_oom_many_locks) {
			G.forker_thargs[i].type = (i == nr_threads - 1 ? CHILD_GREEDY : CHILD_CONTEND);
			G.forker_thargs[i].lock_index = i;
		}

		if (pthread_create(&G.forkers[i], NULL, forker_main, (void *)&G.forker_thargs[i])) {
			perror("pthread_create failed");
			return 1;
		}
	}

	return 0;
}

static int join_threads(void)
{
	set_thread_should_stop();

	for (int i = 0; i < G.nr_forkers; i++)
		if (pthread_join(G.forkers[i], NULL)) {
			perror("pthread_join");
			return 1;
		}

	return 0;
}

static int gather_and_validate_system_information(void) {
	struct sysinfo info;

	if (!legacy_memcg_available()) {
		printf("Legacy memcg (cgroup v1) not available. Try \"./force_cgroup_v1.sh\"\n");
		return 1;
	}

	if (!running_as_root()) {
		printf("Must run program as root!\n");
		return 1;
	}

	if ((G.pagesize = sysconf(_SC_PAGESIZE)) < 1) {
		printf("WARNING: unable to detect pagesize, defaulting to 4K.\n");
		G.pagesize = four_kilobytes;
	}

	if (sysinfo(&info)) {
               perror("sysinfo failed");
	       return 1;
	}
	G.totalram = info.totalram;
	G.freeram = info.freeram;

	return 0;
}

static int setup(void)
{
	size_t i;

	if (gather_and_validate_system_information()) {
		printf("Unable to verify and collect system details. Quit.\n");
		return 1;
	}

	if (!(G.forkers = malloc(sizeof(pthread_t) * G.nr_forkers))) {
		perror("Malloc of pthreads failed");
		return 1;
	}


	if (mmap_anon_shared((void *)&G.robust_locks, sizeof(pthread_mutex_t) * G.nr_robust_locks)) {
		printf("Failed to allocate an array of mutex lock pointers. Cannot go on with living!\n");
		return 1;
	}

	for (i = 0; i < G.nr_robust_locks; i++)
		if (setup_robust_lock(&(G.robust_locks[i]))) {
			printf("Failed to setup a robust lock!\n");
			return 1;
		}

	if (setup_memcg()) {
		printf("Failed to setup memory cgroup on legacy hierarchy\n");
		return 1;
	}

	// Must save the top-level PID to to a file for scripting
	if (!(G.master_pid_file= fopen("master_pid", "w"))) {
		printf("unable to open master_pid file, repro scripts will fail\n") ;
		return 1;
	} else {
		fprintf(G.master_pid_file, "%d\n", getpid()) ;
		fflush(G.master_pid_file) ;
		fclose(G.master_pid_file) ;
	}

	return 0;
}

static int cleanup(void)
{
	size_t i;

	cleanup_memcg();

	if (pthread_mutex_destroy(G.stop_lock)) {
		perror("failed to destroy a mutex");
		return 1;
	}

	for (i = 0; i < G.nr_robust_locks; i++)
		if (pthread_mutex_destroy(G.robust_locks[i])) {
			perror("failed to destroy a mutex");
			return 1;
		}

	free(G.stop_lock);
	free(G.forkers);
	free(G.forker_thargs);

	return 0;
}

void help(char * progname) {
	printf("Usage: %s [-f <nr_forkers> | -l <nr_robust_locks> |\n"
		"-p <nr_private_locks> | -g <greediness> | -s <sleepiness> | -o | -d ]\n"
		"\t<nr_forkers> specifies the number of threads forking children\n"
		"\tIf no value is supplied, <nr_forkers> defaults to %d.\n"
		"\tIf a negative value is supplied, the main thread sleeps indefinitely\n"
		"\t<nr_robust_locks> specifies the number of robust locks allocated\n"
		"\tIf no value is supplied, <nr_locks> defaults to %d.\n"
		"\t<nr_robust_locks> specifies the number of private locks allocated\n"
		"\tfor the greedy child.\n"
		"\tIf no value is supplied, <nr_private_locks> defaults to %d.\n"
		"\t<greediness> specifies how greedy the greedy child is. See below\n"
		"\tIf no value is supplied, <greediness> defaults to %d.\n"
		"\t<sleepiness> specifies how many seconds the main thread sleeps for\n"
		"\tbefore joining threads. Set to zero to never sleep.\n"
		"\tIf no value is supplied, <sleepiness> defaults to %d.\n"
		"\t-d option will do everything but actually touch memory\n"
		"\t-o option will get funky in a particular way:\n"
		"\t\t<nr_robust_locks> will be set to at a minimum to <nr_forkers>\n"
		"\t\tThe forkers will fork() children that contend their own locks but\n"
		"\t\tdo not touch memory. One additional thread will fork() \"greedy\"\n"
		"\t\tchildren that acquire all locks before touching memory. Furthermore,\n"
		"\t\tif <nr_private_locks> > 0, the greedy child will also lock this many\n"
		"\t\tprivate|anon mmaped pthread_mutex (non-robust) locks before touching\n"
		"\t\tmemory. To make the child a bit faster, specify a <greediness> value\n"
		"\t\tto increase the number of times the greedy child touches memory per\n"
		"\t\ttime it manages to acquire all of the necessary locks. Funky.\n",
		progname,
		DEFAULT_NR_FORKERS,
		DEFAULT_NR_ROBUST_LOCKS,
		DEFAULT_NR_PRIVATE_LOCKS,
		DEFAULT_GREEDINESS,
		DEFAULT_SLEEPINESS);
}

int main(int argc, char *argv[])
{
	int opt, tmp;

	while ((opt = getopt(argc, argv, "l:f:p:g:s:vdoh")) != -1) {
		switch (opt) {
		case 'f':
			if ((tmp = atoi(optarg)) != 0 ) {
				G.nr_forkers = tmp;
			} else if (!strncmp(optarg, "0", 1)) {
				// we accept zero
				G.nr_forkers = 0;
			}
			break;
		case 'l':
			if ((tmp = atoi(optarg)) != 0 ) {
				G.nr_robust_locks = tmp;
			} else if (!strncmp(optarg, "0", 1)) {
				// we accept zero
				G.nr_robust_locks = 0;
			}
			break;
		case 'p':
			if ((tmp = atoi(optarg)) != 0 ) {
				G.nr_private_locks = tmp;
			} else if (!strncmp(optarg, "0", 1)) {
				// we accept zero
				G.nr_private_locks = 0;
			}
			break;
		case 'g':
			if ((tmp = atoi(optarg)) != 0 ) {
				G.greediness = tmp;
			} else if (!strncmp(optarg, "0", 1)) {
				// we accept zero
				G.greediness = 0;
			}
			break;
		case 's':
			if ((tmp = atoi(optarg)) != 0) {
				G.sleepiness = tmp;
			} else if (!strncmp(optarg, "0", 1)) {
				// we accept zero
				G.sleepiness = 0;
			}
			break;
		case 'v':
			G.verbosity = true;
			break;
		case 'd':
			G.do_not_touch = true;
			break;
		case 'o':
			G.one_oom_many_locks = true;
			break;
		case 'h':
			help(argv[0]);
			return 0;
			break;
		case '?':
			printf("Unknown option '-%c'. Ignoring.\n", optopt);
			break;
		}
	}

	printf("[%d] Start main thread\n", getpid());

	if (G.verbosity)
		printf("[config]\n"
			"do_not_touch=%d\n"
			"one_oom_many_locks=%d\n"
			"verbosity=%d\n"
			"nr_robust_locks=%zu\n"
			"nr_forkers=%zu\n"
			"nr_private_locks=%zu\n"
			"sleepiness=%d\n",
			G.do_not_touch,
			G.one_oom_many_locks,
			G.verbosity,
			G.nr_robust_locks,
			G.nr_forkers,
			G.nr_private_locks,
			G.sleepiness);

	/*
	 * If we are doing one oom thread with many locks
	 * we need the number of locks to be at least as great as
	 * the number of forkers
	 */
	if (G.one_oom_many_locks) {
		if (G.nr_robust_locks < G.nr_forkers) {
			printf("[one_oom_many_locks] Raising robust_locks from %zu to %zu to match forker count\n", G.nr_robust_locks, G.nr_forkers);
			G.nr_robust_locks = G.nr_forkers;
		}
	}

	if (setup()) {
		printf("Failed to setup reproduction environment. Quit.\n");
		return 1;
	}


	/*
	 * Create threads that fork children that rapidly oom while contending
	 * for a shared lock backed by robust futex
	 */
	if (create_threads()) {
		printf("Failed to create forker threads. Something is wrong. Quit.\n");
	}

	if (G.sleepiness > 0) {
		sleep(G.sleepiness);
	} else if (G.sleepiness < 0) {
		/* a deep and dreamless sleep */
		for (;;);
	}
			     

	pthread_mutex_lock(G.robust_locks[0]);
	pthread_mutex_unlock(G.robust_locks[0]);

	if (join_threads()) {
		printf("Failed to join with pthreads. Something is wrong. Quit.\n");
		return 1;
	}

	if (cleanup()) {
		printf("failed to cleanup reproduction environment. Failed failure?\n");
		return 1;
	}

	printf("===[FAILED REPRODUCTION]===\n");

	/*
	 * Successful failure :)
	 */
	return 0;
}
