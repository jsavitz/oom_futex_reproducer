#!/bin/bash

# checks for bug repro conditions present on system

BIN=oom_futex_reproducer

# gdb requried
if ! command -v gdb &> /dev/null
then
	echo "gdb not found"
	exit 1
fi


master=`cat master_pid`

# use gdb script in file "getinfo" to get pthread_mutex_t structure data from hung master
gdbraw=$(gdb ${BIN} --command=getinfo "$master" ;reset)

# the value we are looking for is the 13th field of the raw gdb output
# the record starts with $1 as it is gdb's first result
deadowner=$(echo "$gdbraw" | awk '/\$1/ {print $13}' | cut -d, -f1)

if [ ! -z $deadowner ];
then
	# keep last dmesg output in a log
	dmesg | grep "oom_reaper: reaped process $deadowner" > log.txt
	reproduced=$?
else
	# no dead owner => no reproduction
	reproduced=1
fi


if [ ${reproduced} ]; then
	echo "Process ${master} hung waiting for futex allegedly owned by oom-killed process ${deadowner}"
	echo "The obsevation of these conditions fulfills our neccessary reproduction criteria"
	echo "===[CONFIRMED REPRODUCTION]==="
else
	echo "Unable to locate oom_reaper message killing alleged lock holder ${deadowner}"
	echo "This may mean that the the bug has failed to reproduce"
	echo "However it may also mean that the system is not configured properly"
fi

