CC 	= gcc
CFLAGS  = -g -Wall -Werror -std=gnu11 -lpthread
OBJECTS = oom_futex_reproducer.o
BIN	= oom_futex_reproducer

all: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(BIN)

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

.PHONEY: clean
clean:
	rm -rf $(BIN) $(OBJECTS)
